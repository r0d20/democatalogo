package com.r0d20.democatalogo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rodrigo Gutierrez on 13/10/2016.
 */
public class SQLHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "Inventario.db";
    public static final String TABLA_MATERIAL = "Material";
    public static final String TABLA_ARTICULO = "Articulo";
    public static final String COL_ID = "_id";
    public static final String COL_NOMBRE = "Nombre";
    public static final String COL_PRECIO = "Precio";
    public static final String COL_MEDIDA = "Medida";
    public static final String COL_MATERIAL_FK = "MaterialFK";
    public static final String ALIAS_MaterialFK = "Material";

    public static final String []listaMateriales = {"Cobre","PVC","CPVC","Aluminio"};

    public SQLHelper(Context context){ super(context, DATABASE_NAME, null, 1);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREA_TABLA_MATERIAL = "CREATE TABLE " + TABLA_MATERIAL +" ("+
                COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                COL_NOMBRE +" TEXT NOT NULL)";
        db.execSQL(CREA_TABLA_MATERIAL);

        String CREA_TABLA_ARTICULO = "CREATE TABLE " + TABLA_ARTICULO +" ("+
                COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                COL_NOMBRE +" TEXT NOT NULL,"+
                COL_MEDIDA+" TEXT NOT NULL,"+
                COL_PRECIO+" REAL DEFAULT 0,"+
                COL_MATERIAL_FK+" INTEGER REFERENCES Material(_id) ON DELETE NO ACTION NOT NULL)";//fk material
        db.execSQL(CREA_TABLA_ARTICULO);

        ContentValues values = new ContentValues();
        values.put(COL_NOMBRE,listaMateriales[0]);//bd 1, cobre
        db.insert(TABLA_MATERIAL, null, values);
        values.put(COL_NOMBRE,listaMateriales[1]);//bd 2, pvc1
        db.insert(TABLA_MATERIAL, null, values);
        values.put(COL_NOMBRE,listaMateriales[2]);//bd 3, cpvc
        db.insert(TABLA_MATERIAL, null, values);
        values.put(COL_NOMBRE,listaMateriales[3]);//bd 4, aluminio
        db.insert(TABLA_MATERIAL, null, values);
        values.clear();

        values.put(COL_NOMBRE,"Tubo");
        values.put(COL_MEDIDA,"1/2 pulgada x 3M");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,30);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,20);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,15);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,35);
        db.insert(TABLA_ARTICULO, null, values);


        values.put(COL_NOMBRE,"Tubo");
        values.put(COL_MEDIDA,"1/4 pulgada x 3M");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,25);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,18);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,12);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,30);
        db.insert(TABLA_ARTICULO, null, values);



        values.put(COL_NOMBRE,"Codo");
        values.put(COL_MEDIDA,"1/2 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,6);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,3);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,3);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,8);
        db.insert(TABLA_ARTICULO, null, values);


        values.put(COL_NOMBRE,"Codo");
        values.put(COL_MEDIDA,"1/4 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,5.5f);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,2.5f);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,2.5f);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,7);
        db.insert(TABLA_ARTICULO, null, values);



        values.put(COL_NOMBRE,"Conexion T");
        values.put(COL_MEDIDA,"1/2 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,10);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,8);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,7);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,15);
        db.insert(TABLA_ARTICULO, null, values);


        values.put(COL_NOMBRE,"Conexion T");
        values.put(COL_MEDIDA,"1/4 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,9);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,6);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,6);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,11);
        db.insert(TABLA_ARTICULO, null, values);



        values.put(COL_NOMBRE,"Conexion X");
        values.put(COL_MEDIDA,"1/2 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,12);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,10);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,10);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,16);
        db.insert(TABLA_ARTICULO, null, values);


        values.put(COL_NOMBRE,"Conexion X");
        values.put(COL_MEDIDA,"1/4 pulgada");
        values.put(COL_MATERIAL_FK,1);//cobre
        values.put(COL_PRECIO,10);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,2);//pvc
        values.put(COL_PRECIO,8);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,3);//cpvc
        values.put(COL_PRECIO,9);
        db.insert(TABLA_ARTICULO, null, values);

        values.put(COL_MATERIAL_FK,4);//aluminio
        values.put(COL_PRECIO,13);
        db.insert(TABLA_ARTICULO, null, values);

        values.clear();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLA_MATERIAL);
        db.execSQL("DROP TABLE IF EXISTS "+TABLA_ARTICULO);
        onCreate(db);
    }
}
