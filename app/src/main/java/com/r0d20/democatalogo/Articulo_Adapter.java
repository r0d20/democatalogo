package com.r0d20.democatalogo;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rodrigo Gutierrez on 13/10/2016.
 */
public class Articulo_Adapter extends RecyclerView.Adapter<Articulo_Adapter.Holder> {

    Context context;
    Drawable [] iconos;
    List<Articulo> listaArticulos = new ArrayList<>();

    public List<Articulo> getListaArticulos() {
        return listaArticulos;
    }
    public void setListaArticulos(List<Articulo> listaArticulos) {this.listaArticulos = listaArticulos;}

    private Cursor cursorArticulos;
    public Cursor getCursorArticulos() {
        return cursorArticulos;
    }
    public void swapCursor(Cursor nuevo){
        if(nuevo != null) {
            cursorArticulos = nuevo;
            notifyDataSetChanged();
        }
    }

    public void Shutdown(){
        cursorArticulos.close();
    }

    public Articulo_Adapter (){}

    public Articulo_Adapter (List<Articulo> listado,Context context){
        listaArticulos = listado;
        iconos = new Drawable[4];

        Resources resources = context.getResources();
        iconos[0]=ResourcesCompat.getDrawable(resources, R.drawable.ic_tubo,null);
        iconos[1]=ResourcesCompat.getDrawable(resources, R.drawable.ic_codo,null);
        iconos[2]=ResourcesCompat.getDrawable(resources, R.drawable.ic_con_t,null);
        iconos[3]=ResourcesCompat.getDrawable(resources, R.drawable.ic_con_x,null);
        this.context = context;
    }

    public Articulo_Adapter (Cursor datos,Context context){
        cursorArticulos = datos;
        iconos = new Drawable[4];

        Resources resources = context.getResources();
        iconos[0]=ResourcesCompat.getDrawable(resources, R.drawable.ic_tubo,null);
        iconos[1]=ResourcesCompat.getDrawable(resources, R.drawable.ic_codo,null);
        iconos[2]=ResourcesCompat.getDrawable(resources, R.drawable.ic_con_t,null);
        iconos[3]=ResourcesCompat.getDrawable(resources, R.drawable.ic_con_x,null);
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.articulo_detalles, parent, false);
        Holder holder = new Holder(v);
        v.setOnClickListener(holder);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
//        Articulo a = listaArticulos.get(position);
        cursorArticulos.moveToPosition(position);
        Articulo a = new Articulo();
        a.setNombre(cursorArticulos.getString(cursorArticulos.getColumnIndex(SQLHelper.COL_NOMBRE)));
        a.setMedida(cursorArticulos.getString(cursorArticulos.getColumnIndex(SQLHelper.COL_MEDIDA)));
        a.setMaterial(cursorArticulos.getString(cursorArticulos.getColumnIndex(SQLHelper.ALIAS_MaterialFK)));
        a.setPrecio(cursorArticulos.getFloat(cursorArticulos.getColumnIndex(SQLHelper.COL_PRECIO)));


        holder.articulo_descripcion.setText(a.toString());
        holder.articulo_precio.setText(String.format("Precio: %.4f",a.getPrecio()));

        if(a.getNombre().contains("Tubo")){
            holder.articulo_icono.setImageDrawable(iconos[0]);
        }
        else if(a.getNombre().contains("Codo")){
            holder.articulo_icono.setImageDrawable(iconos[1]);

        }
        else if(a.getNombre().contains("Conexion T")){
            holder.articulo_icono.setImageDrawable(iconos[2]);
        }
        else if(a.getNombre().contains("Conexion X")){
            holder.articulo_icono.setImageDrawable(iconos[3]);
        }
    }

    @Override
    public int getItemCount() {
//        return listaArticulos.size();
        return cursorArticulos.getCount();
    }



    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView  articulo_descripcion, articulo_precio;
        ImageView articulo_icono;
//      CardView cardview;
        public Holder(View v){
            super(v);
            articulo_descripcion = (TextView)v.findViewById(R.id.art_desc);
            articulo_precio = (TextView)v.findViewById(R.id.art_precio);
            articulo_icono =(ImageView)v.findViewById(R.id.art_icono);
        }

        @Override
        public void onClick(View v) {
        }
    }
}
