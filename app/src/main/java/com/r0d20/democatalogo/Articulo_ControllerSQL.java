package com.r0d20.democatalogo;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo Gutierrez on 13/10/2016.
 */
public class Articulo_ControllerSQL extends ContentProvider{

    private ContentResolver resolver;
    private static SQLHelper helper;
    private int ultimaSeleccion = -1;
    private static Uri ultimaOperacion = null;


    private static final String AUTORIDAD = "com.r0d20.democatalogo";
    private static final Uri CONTENIDO_BASE = Uri.parse("content://"+AUTORIDAD);
    public static final Uri CONTENIDO = CONTENIDO_BASE.buildUpon().appendPath("articulos").build();
    public static final Uri CONTENIDO_MATERIAL = CONTENIDO.buildUpon().appendPath("material").build();

    public static final UriMatcher uriMatcher;
    public static final int ARTICULOS = 10;//todos los articulos
    public static final int ARTICULO_ID = 11;//un articulo
    public static final int ARTICULOS_MATERIAL = 20;//articulos por material
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTORIDAD,"articulos",ARTICULOS);
        uriMatcher.addURI(AUTORIDAD,"articulos/#",ARTICULO_ID);
        uriMatcher.addURI(AUTORIDAD,"articulos/material",ARTICULOS_MATERIAL);
    }

    /*
    Articulo_ControllerSQL(Context context){
        if(helper == null)
            helper = new SQLHelper(context);

        resolver = context.getContentResolver();
    }
*/
    public boolean RepetirSeleccion(int idFiltro){
        if(idFiltro == ultimaSeleccion)
            return true;

        return false;
    }

    public boolean Insertar(Articulo a){
        ContentValues values = new ContentValues();
        values.put(SQLHelper.COL_NOMBRE, a.getNombre());
        values.put(SQLHelper.COL_MEDIDA, a.getMedida());
        values.put(SQLHelper.COL_PRECIO, a.getPrecio());

        String mat = a.getMaterial().toUpperCase();
        if(mat == null) {
            Log.e("Error","Articulo_ControllerSQL.Insertar: material es null");
            return false;
        }
        switch (mat){
            case "COBRE":
                values.put(SQLHelper.COL_MATERIAL_FK, 1);
                break;

            case "PVC":
                values.put(SQLHelper.COL_MATERIAL_FK, 2);
                break;

            case "CPVC":
                values.put(SQLHelper.COL_MATERIAL_FK, 3);
                break;

            case "ALUMINIO":
                values.put(SQLHelper.COL_MATERIAL_FK, 4);
                break;
        }
        long res = helper.getWritableDatabase().insert(SQLHelper.TABLA_ARTICULO, null, values);
        helper.close();
        if(res != -1)
            return false;
        else
            return  true;
    }

    public int Eliminar(int id){
        int res = -1;
        try{
            res = helper.getWritableDatabase()
                    .delete(SQLHelper.TABLA_ARTICULO, SQLHelper.COL_ID+"= ?", new String[]{String.valueOf(id)});
        }
        catch (Exception e){
            Log.e("Error", "Articulo_ControllerSQL.Eliminar: "+e.getMessage());
        }
        finally {
            helper.close();
        }
        return res;    }

    public List<Articulo> ListaArticulos(int idMaterial){//seleccion, idMaterial = -1 si es select * from articulo join material

        String tablaArticuloPunto = SQLHelper.TABLA_ARTICULO+".";
        String tablaMaterialPunto = SQLHelper.TABLA_MATERIAL+".";

        String join = SQLHelper.TABLA_ARTICULO +" JOIN "+SQLHelper.TABLA_MATERIAL +
                " ON "+tablaMaterialPunto+SQLHelper.COL_ID +"="+tablaArticuloPunto+SQLHelper.COL_MATERIAL_FK;

//        select articulo.id, articulo.nombre, articulo.medida, articulo.precio, material.nombre as "material"
//        from articulo join material on material.id = articulo.fk
//        order by articulo.id asc;
        String[] columnas = {
                tablaArticuloPunto+SQLHelper.COL_ID,
                tablaArticuloPunto+SQLHelper.COL_NOMBRE,
                tablaArticuloPunto+SQLHelper.COL_MEDIDA,
                tablaArticuloPunto+SQLHelper.COL_PRECIO,
                tablaMaterialPunto+SQLHelper.COL_NOMBRE +" as "+SQLHelper.ALIAS_MaterialFK
        };

        String selection = null;
        String[] selectionArgs = null;
        if(idMaterial > -1) {
            selection = tablaMaterialPunto + SQLHelper.COL_ID + "= ?";
            selectionArgs = new String[]{String.valueOf(idMaterial)};
        }
        List<Articulo> listaRes = new ArrayList<>();
        Cursor cursor = null;
        try{
            cursor = helper.getReadableDatabase().query(join,
                    columnas,
                    selection,
                    selectionArgs,
                    null, null,tablaArticuloPunto+SQLHelper.COL_ID);

            int [] indices = new int[cursor.getColumnCount()];
            if(indices.length < 4) {
                Log.e("Activity", "CargaArticulos: numero invalido de columnas devuelto por query en ControllerSQL.ListaArticulos");
                return null;
            }

            indices[0] = cursor.getColumnIndex(SQLHelper.COL_NOMBRE);
            indices[1] = cursor.getColumnIndex(SQLHelper.COL_MEDIDA);
            indices[2] = cursor.getColumnIndex(SQLHelper.ALIAS_MaterialFK);
            indices[3] = cursor.getColumnIndex(SQLHelper.COL_PRECIO);

            if(!cursor.moveToFirst()){
                Log.e("ControllerSQL", "CargaArticulos: base de datos vacia");
                return null;
            }
            Articulo articulo;
            while(!cursor.isAfterLast()){
                articulo = new Articulo();
                articulo.setNombre(cursor.getString(indices[0]));
                articulo.setMedida(cursor.getString(indices[1]));
                articulo.setMaterial(cursor.getString(indices[2]));
                articulo.setPrecio(cursor.getFloat(indices[3]));
                listaRes.add(articulo);
                cursor.moveToNext();
            }
            if(!cursor.isClosed())
                cursor.close();
        }
        catch (Exception e){
            Log.e("Error", "Articulo_ControllerSQL.ListaArticulos: "+e.getMessage());
        }
        finally {
            helper.close();
        }
        ultimaSeleccion = idMaterial;
        return listaRes;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        if(helper == null)
            helper = new SQLHelper(context);

        resolver = context.getContentResolver();

        Log.d("Activity","content provider create");
        return true;
    }

    @Override
    public void shutdown() {
        helper = null;
        resolver = null;
        super.shutdown();
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case ARTICULOS:
//                Log.d("Activity","get type articulos");
                return "vnd.android.cursor.dir/vnd."+AUTORIDAD;
            case ARTICULO_ID:
                return "vnd.android.cursor.item/vnd."+AUTORIDAD;
            case ARTICULOS_MATERIAL:
//                Log.d("Activity","get type material");
                return "vnd.android.cursor.dir/vnd."+AUTORIDAD+"/material";
            default:
                throw new IllegalArgumentException("Uri desconocida: "+uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        //evitar seleccion repetida
        if(selectionArgs == null)//seleccion de todos
            ultimaSeleccion = 0;

        else if(selectionArgs.length == 1 && ultimaSeleccion == Integer.valueOf(selectionArgs[0]) &&  uri.compareTo(ultimaOperacion)==0) {
            return null;
        }

        String tablaArticuloPunto = SQLHelper.TABLA_ARTICULO+".";
        String tablaMaterialPunto = SQLHelper.TABLA_MATERIAL+".";

        String join = SQLHelper.TABLA_ARTICULO +" JOIN "+SQLHelper.TABLA_MATERIAL +
                " ON "+tablaMaterialPunto+SQLHelper.COL_ID +"="+tablaArticuloPunto+SQLHelper.COL_MATERIAL_FK;

        String[] columnas = {
                tablaArticuloPunto+SQLHelper.COL_ID,
                tablaArticuloPunto+SQLHelper.COL_NOMBRE,
                tablaArticuloPunto+SQLHelper.COL_MEDIDA,
                tablaArticuloPunto+SQLHelper.COL_PRECIO,
                tablaMaterialPunto+SQLHelper.COL_NOMBRE +" as "+SQLHelper.ALIAS_MaterialFK
        };

//        String tipoUri = getType(uri);

        String id = null;
        switch (uriMatcher.match(uri)){
            case ARTICULO_ID:
                id = uri.getPathSegments().get(1);
                selection = tablaArticuloPunto + SQLHelper.COL_ID + "= ?";
                selectionArgs = new String[]{id};
                ultimaSeleccion = Integer.valueOf(id);
                ultimaOperacion = uri;
                break;

            case ARTICULOS_MATERIAL://spinner
                selection = tablaMaterialPunto + SQLHelper.COL_ID + "= ?";
                ultimaSeleccion = Integer.valueOf(selectionArgs[0]);//nunca es null
                ultimaOperacion = uri;
                //selectionArgs = new String[]{id};
                break;
        }

        Cursor cursor = null;
        try{

            cursor = helper.getReadableDatabase().query(join,
                    columnas,
                    selection,
                    selectionArgs,
                    null, null,tablaArticuloPunto+SQLHelper.COL_ID);


            if(!cursor.moveToFirst()){
                Log.e("ControllerSQL", "CargaArticulos: base de datos vacia");
                return null;
            }
        }
        catch (Exception e){
            Log.e("Error", "Articulo_ControllerSQL.Query: "+e.getMessage());
        }
        finally {
            helper.close();
        }


//        resolver.notifyChange(uri,null);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
