package com.r0d20.democatalogo;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class CatalogoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, LoaderManager.LoaderCallbacks<Cursor> {

    Articulo_ControllerSQL controllerSQL;
    Button btn_cobre, btn_pvc1, btn_pvc2, btn_aluminio;
    RecyclerView rv;
    Spinner sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //controllerSQL = new Articulo_ControllerSQL(this);
        //controllerSQL = new Articulo_ControllerSQL();//content provider
/*
        btn_cobre = (Button)findViewById(R.id.button);
        btn_pvc1 = (Button)findViewById(R.id.button2);
        btn_pvc2 = (Button)findViewById(R.id.button3);
        btn_aluminio = (Button)findViewById(R.id.button4);

        btn_cobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                        getContentResolver().query(Articulo_ControllerSQL.CONTENIDO_MATERIAL,null,null,new String[]{"1"},null)
                );
                rv.getLayoutManager().smoothScrollToPosition(rv, null, 0);

//                if(!controllerSQL.RepetirSeleccion(1))
//                    CargaArticulos(1);
//                else
//                    Log.d("Activity","seleccion repetida");
            }
        });
        btn_pvc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                        getContentResolver().query(Articulo_ControllerSQL.CONTENIDO_MATERIAL,null,null,new String[]{"2"},null)
                );
                rv.getLayoutManager().smoothScrollToPosition(rv, null, 0);
//                if(!controllerSQL.RepetirSeleccion(2))
//                    CargaArticulos(2);
//                else
//                    Log.d("Activity","seleccion repetida");
            }
        });
        btn_pvc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                        getContentResolver().query(Articulo_ControllerSQL.CONTENIDO_MATERIAL,null,null,new String[]{"3"},null)
                );
                rv.getLayoutManager().smoothScrollToPosition(rv, null, 0);
//                if(!controllerSQL.RepetirSeleccion(3))
//                    CargaArticulos(3);
//                else
//                    Log.d("Activity","seleccion repetida");
            }
        });
        btn_aluminio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                        getContentResolver().query(Articulo_ControllerSQL.CONTENIDO_MATERIAL,null,null,new String[]{"4"},null)
                );
                rv.getLayoutManager().smoothScrollToPosition(rv, null, 0);
//                if(!controllerSQL.RepetirSeleccion(4))
//                    CargaArticulos(4);
//                else
//                    Log.d("Activity","seleccion repetida");
            }
        });

        btn_cobre.setText(SQLHelper.listaMateriales[0]);
        btn_pvc1.setText(SQLHelper.listaMateriales[1]);
        btn_pvc2.setText(SQLHelper.listaMateriales[2]);
        btn_aluminio.setText(SQLHelper.listaMateriales[3]);
*/
        rv=(RecyclerView)findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
//        rv.setLayoutManager(new GridLayoutManager(this,2));

        getSupportLoaderManager().restartLoader(1,null,this);
//        CargaArticulos(-1);


        sp = (Spinner)findViewById(R.id.spinner);
        sp.setOnItemSelectedListener(this);
        String [] mats = new String[5];
        mats [0]="Todos";
        for(int i=1; i<mats.length; i++)
            mats[i]=SQLHelper.listaMateriales[i-1];
        ArrayAdapter <String> spa =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mats);
        spa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(spa);
        //rv.setAdapter(new Articulo_Adapter());//no hay articulos, lista desde CargaArticulos
//        rv.setAdapter(new Articulo_Adapter(controllerSQL.ListaArticulos(-1)));//lista desde controllerSQL
    }

    @Deprecated
    void CargaArticulos(int idMaterial)
    {
        Log.d("Activity","CargaArticulos no debe ser llamado");

        List<Articulo>lista = new ArrayList<>();

        Cursor cursor = controllerSQL.query(Articulo_ControllerSQL.CONTENIDO,null,null,null,null);
        int [] indices = new int[cursor.getColumnCount()];
        if(indices.length < 4) {
            Log.e("Activity", "CargaArticulos: numero invalido de columnas devuelto por query en ControllerSQL.ListaArticulos");
            return;
        }
        indices[0] = cursor.getColumnIndex(SQLHelper.COL_NOMBRE);
        indices[1] = cursor.getColumnIndex(SQLHelper.COL_MEDIDA);
        indices[2] = cursor.getColumnIndex(SQLHelper.ALIAS_MaterialFK);
        indices[3] = cursor.getColumnIndex(SQLHelper.COL_PRECIO);

        Articulo articulo;
        while(!cursor.isAfterLast()){
            articulo = new Articulo();
            articulo.setNombre(cursor.getString(indices[0]));
            articulo.setMedida(cursor.getString(indices[1]));
            articulo.setMaterial(cursor.getString(indices[2]));
            articulo.setPrecio(cursor.getFloat(indices[3]));
            lista.add(articulo);
            cursor.moveToNext();
        }
        if(!cursor.isClosed())
            cursor.close();

        rv.setAdapter(new Articulo_Adapter(lista, this));

        /*
        List<Articulo>lista = controllerSQL.ListaArticulos(idMaterial);
        rv.setAdapter(new Articulo_Adapter(lista, this));
        */
    }

    @Override
    protected void onDestroy() {
        ((Articulo_Adapter)rv.getAdapter()).Shutdown();
        rv.setAdapter(null);

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_catalogo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ///Spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        Log.d("Activity","seleccionado "+parent.getItemAtPosition(position));
        //CargaArticulos(position-1);
        if(id > 0)
            ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                    getContentResolver().query(Articulo_ControllerSQL.CONTENIDO_MATERIAL,null,null,new String[]{String.valueOf(id)},null)
            );
        else
            ((Articulo_Adapter)rv.getAdapter()).swapCursor(
                    getContentResolver().query(Articulo_ControllerSQL.CONTENIDO,null,null,null,null)
            );

        rv.getLayoutManager().smoothScrollToPosition(rv, null, 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    ///Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.CursorLoader(this,Articulo_ControllerSQL.CONTENIDO, null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        rv.setAdapter(new Articulo_Adapter(data, this));
        Toast.makeText(this,"on Load Finished", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Toast.makeText(this,"on Loader Reset", Toast.LENGTH_SHORT).show();
    }
}
