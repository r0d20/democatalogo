package com.r0d20.democatalogo;

/**
 * Created by Rodrigo Gutierrez on 13/10/2016.
 */
public class Articulo {
    String medida;
    String nombre;
    String material;
    float precio;

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return getNombre() + "\n\nMaterial: "+getMaterial()+"\nMedida: "+getMedida();
    }
}
